Um somador e subtrator supimpa!
- Lucas Jose Kreutz Alves, 2011

Trabalho da Cadeira Arquitetura de Computadores - Intel 8086
Ciencia da Computacao, UFRGS - 2011/2
-------------------------------------------------------------------------------

Ler operacoes em um arquivo fornecido pelo usuario e salvar o resultado delas
em um arquivo de saida.

Instrucoes:
-------------------------------------------------------------------------------
-Baixe e instale o DOSBOX (http://www.dosbox.com/)
-Baixe e descompacte em "c:\tasm" do seu computador o TASM e o TLINK da Borland
(ja foi transformado em freeware pela empresa, encontra-se para baixar na pagina
mais proxima do google)
-Coloque nessa mesma pasta o arquivo "L174021.ASM", "L174021.TXT" e "TST.TXT"
-Abra o DOSBOX e digite: "mount c c:\tasm" e depois "c:"
-Rode o comando "tasm -l L174021"
-Rode o comando "tlink L174021"
-Rode o executavel digitando "L174021", pressione enter, e contemple!


Observacoes:
-------------------------------------------------------------------------------
-> Belissimo layout lembrando a tela azul da morte;
- Roda em outros sistemas, pois o DOSBOX eh multiplataforma (claro, com as
devidas alteracoes na maneira de copiar os arquivos nos diretorios...);
- Torna grande parte das calculadoras conhecidas atualmente obsoletas, pois
esse programa realiza a soma e subtracao de numeros de ate 74 digitos! E todos
sabem que as unicas operacoes realmente supimpas sao essas.